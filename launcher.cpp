// x86_64-w64-mingw32-g++ -o ${TARGET}.exe -Wall -municode -static launcher.cpp
// e.g.: x86_64-w64-mingw32-g++ -o pacman.exe -Wall -municode -static launcher.cpp

#include <windows.h>

#include <string>
#include <vector>

#include <errno.h>
#include <process.h>
#include <stdio.h>
#include <stdlib.h>

namespace {
  const auto msys2_folder = L"msys64";

  void
  parse_path(const wchar_t* path,
             std::wstring& msys2_root,
             std::wstring& target_program_name)
  {
    auto path_length = wcslen(path);

    size_t last_path_separator_position = path_length;
    for (;
         last_path_separator_position > 0;
         --last_path_separator_position) {
      if (path[last_path_separator_position] == L'/' ||
          path[last_path_separator_position] == L'\\') {
        break;
      }
    }

    msys2_root.clear();
    if (last_path_separator_position > 0) {
      if (path[0] != L'\\') { // "\\?\" prefix isn't used
        msys2_root += L"\\\\?\\";
      }
      msys2_root.append(path, last_path_separator_position);
      msys2_root += L"\\";
    }
    msys2_root += msys2_folder;

    size_t target_program_name_start_position;
    if (last_path_separator_position == 0) {
      target_program_name_start_position = 0;
    } else {
      target_program_name_start_position = last_path_separator_position + 1;
    }
    size_t extension_position = path_length;
    for (;
         extension_position > target_program_name_start_position;
         --extension_position) {
      if (path[extension_position] == L'.') {
        break;
      }
    }
    target_program_name.clear();
    if (extension_position == last_path_separator_position) {
      // No extension.
      target_program_name += path + target_program_name_start_position;
    } else {
      target_program_name.append(
        path + target_program_name_start_position,
        extension_position - target_program_name_start_position);
    }
  }

  std::wstring
  bash_escape(const std::wstring& value)
  {
    std::wstring escaped_value;
    for (auto character : value) {
      switch (character) {
      case L'"':
      case L'\\':
      case L'(':
      case L')':
      case L'[':
      case L']':
      case L'{':
      case L'}':
      case L'<':
      case L'>':
      case L';':
      case L'&':
      case L'|':
      case L'#':
      case L'!':
      case L'$':
      case L'~':
      case L'?':
      case L' ':
        escaped_value += L'\\';
        break;
      }
      escaped_value += character;
    }
    return escaped_value;
  }

  std::wstring
  generate_bash_script(const std::wstring& msys2_root,
                       const std::wstring& target_program_name,
                       int n_args,
                       wchar_t** args)
  {
    std::wstring script;
    script += L"WLI_PROGRAM=" + bash_escape(target_program_name);
    if (!script.empty()) {
      script += L" && ";
    }
    script += L"WLI_ARGS=()";
    for (int i = 0; i < n_args; ++i) {
      script += L" && WLI_ARGS+=(" + bash_escape(args[i]) + L")";
    }
    const auto rc_path = msys2_root + L"\\" + target_program_name + L".rc";
    if (GetFileAttributesW(rc_path.c_str()) != INVALID_FILE_ATTRIBUTES) {
      if (!script.empty()) {
        script += L" && ";
      }
      script += L". /" + bash_escape(target_program_name + L".rc");
    }
    script += L" && \"${WLI_PROGRAM}\" \"${WLI_ARGS[@]}\"";
    return script;
  }

  std::wstring
  spawn_escape(const std::wstring& value)
  {
    std::wstring escaped_value;
    escaped_value = L"\"";
    for (auto character : value) {
      switch (character) {
      case L'"':
      case L'\\':
        escaped_value += L'\\';
        break;
      }
      escaped_value += character;
    }
    escaped_value += L"\"";
    return escaped_value;
  }
}

extern "C" int
wmain(int argc, wchar_t** argv, wchar_t** env)
{
  wchar_t *absolute_program_path = argv[0];
  SetLastError(ERROR_SUCCESS);
  // https://docs.microsoft.com/en-us/windows/win32/fileio/naming-a-file
#define LONG_MAX_PATH 32767
  wchar_t absolute_program_path_buffer[LONG_MAX_PATH];
  GetModuleFileNameW(NULL, absolute_program_path_buffer, LONG_MAX_PATH);
  if (GetLastError() == ERROR_SUCCESS) {
    absolute_program_path = absolute_program_path_buffer;
  }
#undef LONG_MAX_PATH
  std::wstring msys2_root;
  std::wstring target_program_name;
  parse_path(absolute_program_path, msys2_root, target_program_name);
  auto script = generate_bash_script(msys2_root,
                                     target_program_name,
                                     argc - 1,
                                     argv + 1);

  std::vector<std::wstring> command_line_data;
  command_line_data.push_back(msys2_root + L"\\usr\\bin\\bash.exe");
  command_line_data.push_back(L"--login");
  command_line_data.push_back(L"-c");
  command_line_data.push_back(spawn_escape(script));
  std::vector<const wchar_t*> command_line;
  for (const auto& command_line_argument : command_line_data) {
    command_line.push_back(command_line_argument.c_str());
  }
  command_line.push_back(nullptr);

  std::vector<std::wstring> command_env_data;
  {
    bool have_chere_invoking = false;
    bool have_msys2_path_type = false;
    bool have_msystem = false;
    for (size_t i = 0; env[i]; ++i) {
      std::wstring env_string(env[i]);
      if (env_string.compare(0,
                             strlen("CHERE_INVOKING="),
                             L"CHERE_INVOKING=") == 0) {
        have_chere_invoking = true;
      } else if (env_string.compare(0,
                                strlen("MSYS2_PATH_TYPE="),
                                L"MSYS2_PATH_TYPE=") == 0) {
        have_msys2_path_type = true;
      } else if (env_string.compare(0,
                                    strlen("MSYSTEM="),
                                    L"MSYSTEM=") == 0) {
        have_msystem = true;
      }
      command_env_data.push_back(env[i]);
    }
    if (!have_chere_invoking) {
      command_env_data.push_back(L"CHERE_INVOKING=1");
    }
    if (!have_msys2_path_type) {
      command_env_data.push_back(L"MSYS2_PATH_TYPE=inherit");
    }
    if (!have_msystem) {
      command_env_data.push_back(L"MSYSTEM=MINGW64");
    }
  }
  std::vector<const wchar_t*> command_env;
  for (const auto& command_env_variable : command_env_data) {
    command_env.push_back(command_env_variable.c_str());
  }
  command_env.push_back(nullptr);

  intptr_t exit_status = _wspawnve(_P_WAIT,
                                   command_line[0],
                                   command_line.data(),
                                   command_env.data());
  if (exit_status == -1) {
    std::wstring command_line_string;
    for (const auto command_line_argument : command_line_data) {
      if (!command_line_string.empty()) {
        command_line_string += L" ";
      }
      command_line_string += command_line_argument;
    }
    fwprintf(stderr,
             L"%ls: failed to launch: %s\n"
             L"%ls\n",
             argv[0],
             strerror(errno),
             command_line_string.c_str());
    return EXIT_FAILURE;
  } else {
    return exit_status;
  }
}
