!define PRODUCT_NAME "LuaJIT Installer"
!define PRODUCT_VERSION_MAJOR "1"
!define PRODUCT_VERSION_MINOR "0"
!define PRODUCT_VERSION_MICRO "9"
!define PRODUCT_VERSION "${PRODUCT_VERSION_MAJOR}.${PRODUCT_VERSION_MINOR}.${PRODUCT_VERSION_MICRO}"
!define PRODUCT_PUBLISHER "ClearCode Inc."
!define PRODUCT_WEBSITE "https://clear-code.gitlab.io/windows-luajit-installer/"

!define MSYS2_ARCHITECTURE "x86_64"
!define MSYS2_INSTALLER_TAG "2020-07-20"
!define MSYS2_INSTALLER_VERSION "20200720"
!define MSYS2_INSTALLER_NAME "msys2-base-${MSYS2_ARCHITECTURE}-${MSYS2_INSTALLER_VERSION}.sfx.exe"
!define MSYS2_INSTALLER_URL_BASE "https://github.com/msys2/msys2-installer/releases/download/${MSYS2_INSTALLER_TAG}"

# https://docs.microsoft.com/ja-jp/windows/win32/msi/uninstall-registry-key
!define UNINSTALL_KEY "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_NAME}"
!define UNINSTALL_ROOT_KEY "HKLM"
!define UNINSTALLER_NAME "uninstall.exe"
!define START_MENU_PAGE_ID StartMenu
!define START_MENU_REGISTRY_VALUE_NAME "NSIS:StartMenuDir"

!define REGISTRY_VIEW 64

# https://nsis.sourceforge.io/Docs/Chapter2.html#tutmodernui
!include "MUI2.nsh"
# https://nsis.sourceforge.io/Docs/Chapter2.html#tut-logic
!include "LogicLib.nsh"
# https://nsis.sourceforge.io/Docs/AppendixE.html#filefunc
# For GetTime.
!include "FileFunc.nsh"

# https://nsis.sourceforge.io/Docs/Chapter4.html#aunicodetarget
Unicode true

# https://nsis.sourceforge.io/Docs/Chapter4.html#aname
Name "${PRODUCT_NAME} ${PRODUCT_VERSION}"
# https://nsis.sourceforge.io/Docs/Chapter4.html#ainstalldir
InstallDir "C:\luajit"

# https://nsis.sourceforge.io/Docs/Chapter4.html#ainsttype
# "Full" and "INST_TYPE_FULL" aren't fixed values.
# We can choose any values but we should use the same install
# type ID (INST_TYPE_FULL) for SectionInstType in Section.
InstType "Full" INST_TYPE_FULL

# https://nsis.sourceforge.io/Docs/Modern%20UI%202/Readme.html
!insertmacro MUI_PAGE_WELCOME
!define MUI_LICENSEPAGE_CHECKBOX
!insertmacro MUI_PAGE_LICENSE "LICENSE"
!insertmacro MUI_PAGE_DIRECTORY
Var START_MENU_FOLDER
!define MUI_STARTMENUPAGE_NODISABLE
!define MUI_STARTMENUPAGE_DEFAULTFOLDER "${PRODUCT_NAME}"
!define MUI_STARTMENUPAGE_REGISTRY_ROOT "${UNINSTALL_ROOT_KEY}"
!define MUI_STARTMENUPAGE_REGISTRY_KEY "${UNINSTALL_KEY}"
!define MUI_STARTMENUPAGE_REGISTRY_VALUENAME "${START_MENU_REGISTRY_VALUE_NAME}"
!insertmacro MUI_PAGE_STARTMENU ${START_MENU_PAGE_ID} $START_MENU_FOLDER
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH

!insertmacro MUI_UNPAGE_WELCOME
!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES
!insertmacro MUI_UNPAGE_FINISH

!insertmacro MUI_LANGUAGE "English"
!insertmacro MUI_LANGUAGE "Japanese"
!insertmacro MUI_RESERVEFILE_LANGDLL

Function .onInit
  # https://nsis.sourceforge.io/Docs/Modern%20UI%202/Readme.html
  # See the "Language selection dialog" section.
  !insertmacro MUI_LANGDLL_DISPLAY
FunctionEnd

# https://nsis.sourceforge.io/Docs/Chapter4.html#ssection
# We must have at least one Section.
Section "MSYS2"
  # https://nsis.sourceforge.io/Docs/Chapter4.html#ssectioninsttype
  # "INST_TYPE_FULL" must be the same value specified by InstType.
  SectionInstType ${INST_TYPE_FULL}

  # https://nsis.sourceforge.io/Docs/Chapter4.html#asetoverwrite
  SetOverwrite ifdiff

  # https://nsis.sourceforge.io/Docs/Chapter4.html#setoutpath
  # It changes the current folder.
  SetOutPath "$INSTDIR"

  # https://nsis.sourceforge.io/Docs/Chapter4.html#saddsize
  # This is rough MSYS2 size.
  # We can't estimate it dynamically.
  AddSize 358400 # 350MiB

  ${Unless} ${FileExists} "$INSTDIR\msys64"
    # Cache downloaded MSYS2 installer to $TEMP.
    # $TEMP is C:\Users\${USER}\AppData\Local\Temp.
    ${Unless} ${FileExists} "$TEMP\${MSYS2_INSTALLER_NAME}"
      # https://nsis.sourceforge.io/Docs/Chapter4.html#detailprint
      DetailPrint "Downloading MSYS2 installer"
      # https://nsis.sourceforge.io/NSISdl_plug-in doesn't support https.
      ExecWait \
        'powershell.exe \
           -Command \
             "Invoke-WebRequest \
                -Uri ${MSYS2_INSTALLER_URL_BASE}/${MSYS2_INSTALLER_NAME} \
                -OutFile \"$TEMP\${MSYS2_INSTALLER_NAME}\""'
    ${EndUnless}

    DetailPrint "Extracting MSYS2"
    # https://nsis.sourceforge.io/Docs/Chapter4.html#execwait
    ExecWait '"$TEMP\${MSYS2_INSTALLER_NAME}" -y'
  ${EndUnless}

  # https://nsis.sourceforge.io/Docs/Chapter4.html#file
  File /oname=bash.exe launcher.exe
  File /oname=pacman.exe launcher.exe
  File /oname=luarocks.exe launcher.exe
  File /oname=luajit.exe launcher.exe
  File /oname=msys64\luajit.rc luajit.rc
  File /oname=msys64\luarocks.rc luarocks.rc

  DetailPrint "Updating MSYS2 (phase1)"
  ExecWait \
    '"$INSTDIR\pacman.exe" --sync --refresh --sysupgrade --sysupgrade --noconfirm'
  DetailPrint "Killing MSYS2 related processes (phase1)"
  ExecWait \
    'powershell.exe \
       -WindowStyle Hidden \
       -Command \
         "Get-Process | \
            Where-Object {$$_.Modules.ModuleName -eq \"msys-2.0.dll\"} | \
            Stop-Process"'

  DetailPrint "Updating MSYS2 (phase2)"
  ExecWait \
    '"$INSTDIR\pacman.exe" --sync --sysupgrade --sysupgrade --noconfirm'
  DetailPrint "Killing MSYS2 related processes (phase2)"
  ExecWait \
    'powershell.exe \
       -WindowStyle Hidden \
       -Command \
         "Get-Process | \
            Where-Object {$$_.Modules.ModuleName -eq \"msys-2.0.dll\"} | \
            Stop-Process"'

  File *.pkg.tar.zst
  DetailPrint "Installing custom LuaRocks"
  ExecWait '"$INSTDIR\pacman.exe" --upgrade --noconfirm *.pkg.tar.zst'
  Delete *.pkg.tar.zst

  DetailPrint "Installing packages for building Lua modules"
  ExecWait \
    '"$INSTDIR\pacman.exe" --sync --needed --noconfirm \
       mingw-w64-x86_64-gcc \
       mingw-w64-x86_64-luajit \
       make \
       unzip'

  # https://nsis.sourceforge.io/Docs/Modern%20UI%202/Readme.html
  #
  !insertmacro MUI_STARTMENU_WRITE_BEGIN ${START_MENU_PAGE_ID}
  # https://nsis.sourceforge.io/Docs/Chapter4.html#createdirectory
  CreateDirectory "$SMPROGRAMS\$START_MENU_FOLDER"
  # https://nsis.sourceforge.io/Docs/Chapter4.html#createshortcut
  CreateShortCut "$SMPROGRAMS\$START_MENU_FOLDER\Uninstall.lnk" \
    $INSTDIR\${UNINSTALLER_NAME}
  CreateShortCut "$SMPROGRAMS\$START_MENU_FOLDER\LuaJIT.lnk" \
    "$SYSDIR\cmd.exe" '/C ""$INSTDIR\luajit.exe""'
  CreateShortCut "$SMPROGRAMS\$START_MENU_FOLDER\Bash.lnk" \
    "$INSTDIR\msys64\msys2_shell.cmd" \
    "-mingw64" \
    "$INSTDIR\msys64\mingw64.exe"

  # https://nsis.sourceforge.io/Docs/Chapter4.html#FileOpen
  # $0 is one of the predefined writable variables:
  # https://nsis.sourceforge.io/Docs/Chapter4.html#varother
  FileOpen $0 "$INSTDIR\setup.bat" w
  # https://nsis.sourceforge.io/Docs/Chapter4.html#FileWrite
  FileWrite $0 "set PATH=$INSTDIR;%PATH%$\r$\n"
  # https://nsis.sourceforge.io/Docs/Chapter4.html#FileClose
  FileClose $0
  CreateShortCut "$SMPROGRAMS\$START_MENU_FOLDER\CMD.lnk" \
    "$SYSDIR\cmd.exe" '/E:ON /K ""$INSTDIR\setup.bat""'

  FileOpen $0 "$INSTDIR\run-powershell.bat" w
  FileWrite $0 "set PATH=$INSTDIR;%PATH%$\r$\n"
  FileWrite $0 "start powershell$\r$\n"
  FileClose $0
  CreateShortCut "$SMPROGRAMS\$START_MENU_FOLDER\PowerShell.lnk" \
    "$SYSDIR\cmd.exe" \
    '/E:ON /C ""$INSTDIR\run-powershell.bat""' \
    "$SYSDIR\WindowsPowerShell\v1.0\powershell.exe"

  !insertmacro MUI_STARTMENU_WRITE_END
SectionEnd

# https://nsis.sourceforge.io/Docs/Chapter4.html#ssection
# A section that its name is beginning with "-" is a hidden section.
Section -Post
  # https://nsis.sourceforge.io/Docs/Chapter4.html#writeuninstaller
  WriteUninstaller "$INSTDIR\${UNINSTALLER_NAME}"

  # Register uninstaller.
  # https://docs.microsoft.com/ja-jp/windows/win32/msi/uninstall-registry-key

  # https://nsis.sourceforge.io/Docs/Chapter4.html#setregview
  # It must be 64 for 64bit application.
  SetRegView ${REGISTRY_VIEW}

  # https://nsis.sourceforge.io/Docs/Chapter4.html#writeregstr
  WriteRegStr ${UNINSTALL_ROOT_KEY} "${UNINSTALL_KEY}" \
    "DisplayName" "${PRODUCT_NAME}"
  WriteRegStr ${UNINSTALL_ROOT_KEY} "${UNINSTALL_KEY}" \
    "DisplayVersion" "${PRODUCT_VERSION}"
  WriteRegStr ${UNINSTALL_ROOT_KEY} "${UNINSTALL_KEY}" \
    "Publisher" "${PRODUCT_PUBLISHER}"
  WriteRegStr ${UNINSTALL_ROOT_KEY} "${UNINSTALL_KEY}" \
    "VersionMinor" "${PRODUCT_VERSION_MINOR}"
  WriteRegStr ${UNINSTALL_ROOT_KEY} "${UNINSTALL_KEY}" \
    "VersionMajor" "${PRODUCT_VERSION_MAJOR}"
  WriteRegStr ${UNINSTALL_ROOT_KEY} "${UNINSTALL_KEY}" \
    "Version" "${PRODUCT_VERSION}"
  # https://nsis.sourceforge.io/Docs/AppendixE.html#gettime
  # "SL" means system time in UTC.
  Var /Global DAY
  Var /Global MONTH
  Var /Global YEAR
  Var /Global DAY_OF_WEEK
  Var /Global HOUR
  Var /Global MINUTE
  Var /Global SECOND
  ${GetTime} "" "SL" $DAY $MONTH $YEAR $DAY_OF_WEEK $HOUR $MINUTE $SECOND
  # Format: YYYYMMDDhhmmss.NNNNNN+z
  #   YYYY: Year
  #   MM: Month
  #   DD: Day
  #   hh: Hour
  #   mm: Minute
  #   ss: Second
  #   NNNNNN: Microsecond
  #   z: Timezone in minute
  # Example:
  #   In ISO 8601 format:    2020-07-28:14:26:19.123456+09:00
  #   In InstallDate format: 20200728142619.123456+540
  WriteRegStr ${UNINSTALL_ROOT_KEY} "${UNINSTALL_KEY}" \
    "InstallDate" "$YEAR$MONTH$DAY$HOUR$MINUTE$SECOND.000000+0"
  WriteRegStr ${UNINSTALL_ROOT_KEY} "${UNINSTALL_KEY}" \
    "InstallLocation" "$INSTDIR"
  WriteRegStr ${UNINSTALL_ROOT_KEY} "${UNINSTALL_KEY}" \
    "UninstallString" "$INSTDIR\${UNINSTALLER_NAME}"
SectionEnd

# https://nsis.sourceforge.io/Docs/Chapter4.html#ffunction
# A function that its name is biggening with "un." is a callback function
# for uninstall. "un.onInit" is called on initialize uninstall.
Function un.onInit
  # https://nsis.sourceforge.io/Docs/Modern%20UI%202/Readme.html
  # See the "Settings for registry storage of selected language" section.
  !insertmacro MUI_UNGETLANGUAGE
FunctionEnd

# https://nsis.sourceforge.io/Docs/Chapter4.html#ssection
# The "Uninstall" section is a special section for uninstall.
Section "Uninstall"
  # https://nsis.sourceforge.io/Docs/Chapter4.html#setregview
  # It must be 64 for 64bit application.
  SetRegView ${REGISTRY_VIEW}

  # https://nsis.sourceforge.io/Docs/Chapter4.html#readregstr
  ReadRegStr \
    $START_MENU_FOLDER \
    ${UNINSTALL_ROOT_KEY} \
    "${UNINSTALL_KEY}" \
    "${START_MENU_REGISTRY_VALUE_NAME}"

  # https://nsis.sourceforge.io/Docs/Chapter4.html#rmdir
  RMDir /r "$SMPROGRAMS\$START_MENU_FOLDER"

  DetailPrint "Deleting registry key"
  # https://nsis.sourceforge.io/Docs/Chapter4.html#deleteregkey
  DeleteRegKey ${UNINSTALL_ROOT_KEY} "${UNINSTALL_KEY}"

  DetailPrint "Killing MSYS2 related processes"
  ExecWait \
    'powershell.exe \
       -WindowStyle Hidden \
       -Command \
         "Get-Process | \
            Where-Object {$$_.Modules.ModuleName -eq \"msys-2.0.dll\"} | \
            Stop-Process"'

  RMDir /r $INSTDIR
SectionEnd
