---
title: none
---

<div class="jumbotron">
  <h1>Windows LuaJIT Installer</h1>
  <p class="lead">{{ site.description.en }}</p>
  <p>The latest version
     (<a href="news/#version-{{ site.version | replace:".", "-" }}">{{ site.version }}</a>)
     has been released at {{ site.release_date }}.
  </p>
  <p>
    <a href="install/"
       class="btn btn-primary btn-lg"
       role="button">Install</a>
  </p>
</div>

## Windows LuaJIT Installerについて {#about}

`Windows LuaJIT Installer` は、Windows上でLuaJITを使える環境をセットアッ
プするツールです。[MSYS2][msys2]を用いて環境を構築しています。

`luajit.exe`、`luarocks.exe` という実行ファイルを使って、Luaスクリ
プトを実行したり、LuaRocks上のLuaモジュールをインストールできます。

## ドキュメント {#documentations}

  * [おしらせ][news]: リリース情報。

  * [インストール][install]: Windows LuaJIT Installerを使ったLuaJITのインストール方法。

  * [アンインストール][uninstall]: アンインストール方法。

  * [使い方][usage]: LuaJIT環境の使い方。

  * [アップグレード][upgrade]: アップグレード方法。

## ライセンス {#license}

Windows LuaJIT Installerのライセンスは[MITライセンス][mit-license]です。

著作権保持者など詳細は[LICENSE][license]ファイルを見てください。

[msys2]:https://www.msys2.org/

[news]:news/

[install]:install/

[uninstall]:uninstall/

[usage]:usage/

[upgrade]:upgrade/

[mit-license]:https://opensource.org/licenses/mit

[license]:https://gitlab.com/clear-code/windows-luajit-installer/blob/master/LICENSE
